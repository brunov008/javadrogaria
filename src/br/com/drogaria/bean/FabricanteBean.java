package br.com.drogaria.bean;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.ListDataModel;

import br.com.drogaria.dao.FabricanteDAO;
import br.com.drogaria.domain.Fabricante;

@ManagedBean(name= "MBFabricante")
@SessionScoped
public class FabricanteBean {
	private ListDataModel<Fabricante> itens;
	private Fabricante fabricante;

	public ListDataModel<Fabricante> getItens() {
		return itens;
	}

	public void setItens(ListDataModel<Fabricante> itens) {
		this.itens = itens;
	}
	
	@PostConstruct  //Esse metdo automaticamente chamado antes da pagina ser remasterizada(roda andes de exibir)
	public void prepararPesquisa(){
		try {
		FabricanteDAO dao = new FabricanteDAO();
		ArrayList<Fabricante> lista;
			lista = dao.listar();
		itens = new ListDataModel<Fabricante>(lista);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void prepararNovo(){
		fabricante = new Fabricante();
	}
	
	public void novo(){  //metodo para criar o fabricante 
		try {
			FabricanteDAO dao = new FabricanteDAO();
			dao.salvar(fabricante);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Fabricante getFabricante() {
		return fabricante;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
}
